import { faPencilSquare, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import Helpers from "../Helpers";
import { Badge, Button, Card, Col, Stack } from "react-bootstrap";
import { StartStopTimerButton } from "./StartStopTimerButton";

export const ProjectCard = ({project, onTimerStart, onTimerStop, onEdit, onDelete}) => {
    const [updateCount, setUpdateCount] = useState(0);
    useEffect(() => {
        const interval = setInterval(() => setUpdateCount(updateCount + 1), 100);
        // cleanup
        return () => clearInterval(interval)
    }, [updateCount])

    function handleStartTimer() {
        onTimerStart(project.id);
    }
    function handleStopTimer() {
        onTimerStop(project.id);
    }
    return (
        <Col md={3}>
            <Card border="info">
                <Card.Header>
                    <Stack direction="horizontal">
                        <h4 className="me-auto">{project.name}</h4>
                        <div className="vr" />
                        <p className="my-auto ms-2">${project.price}</p>
                    </Stack>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        <span className="d-block">
                            {project.description}
                        </span>
                        <span className="mt-2 d-block h2 text-center">{Helpers.renderElapsedString(project.elapsed, project.runningSince)}</span>
                    </Card.Text>

                    <Stack direction="horizontal" gap={3} className="mt-4">
                        <Badge pill bg={project.status === "completed" ? 'success' : 'info'} size="md">
                            {project.status}
                        </Badge>
                        <Button
                            variant="outline-danger"
                            className="ms-auto"
                            size="sm"
                            disabled={project.status === "completed"} //depends on product, agar edit aur delete karne dena hai toh do varna mat do
                            onClick={() => onDelete(project.id)}
                        >
                            <FontAwesomeIcon icon={faTrash} />
                        </Button>
                        <Button variant="outline-warning" size="sm" disabled={project.status === "completed"} onClick={() => onEdit(project.id)}>  {/* depends on product, agar edit aur delete karne dena hai toh do varna mat do */}
                            <FontAwesomeIcon icon={faPencilSquare} />
                        </Button>
                    </Stack>
                    <div className="d-grid mt-2">
                        <StartStopTimerButton
                            status={project.status}
                            runningSince={project.runningSince}
                            onTimerStart={handleStartTimer}
                            onTimerStop={handleStopTimer}
                        />
                    </div>
                </Card.Body>
            </Card>
        </Col>
    );
};
